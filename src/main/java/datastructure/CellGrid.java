package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

	private int rows;
	private int cols;
	private ArrayList<CellState> grid;
	private CellState init;
	
	
    public CellGrid(int rows, int cols, CellState initialState) {
    	this.rows = rows;
    	this.cols = cols;
    	this.grid = new ArrayList<CellState>();
    	this.init = initialState;
    	
		for(int row=0; row<rows; row++) {
			for(int col=0; col<cols; col++) {
				grid.add(initialState);
			}
		}
    	
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if(isOnGrid(row,column)) {
    		this.grid.set(indexOf(row, column), element);
    	}
    	else 
			throw new IndexOutOfBoundsException();
		
    }
    
    public boolean isOnGrid(int row, int col) {
		if(row < rows && col < cols && row>=0 && col>=0)
			return true;
		else
			return false;
    }
    
    private int indexOf(int row, int col) {
		if(row<0 || row >= rows)
			throw new IndexOutOfBoundsException("Illegal row value");
		if(col<0 || col >= cols)
			throw new IndexOutOfBoundsException("Illegal col value");
		return row*cols+col;
    }

    @Override
    public CellState get(int row, int column) {
        return this.grid.get(indexOf(row, column));
    }

    @Override
    public IGrid copy() {
    	IGrid gridCopy = new CellGrid(rows, cols, CellState.DEAD);
    	for (int row = 0; row < rows; row++) {
    		for(int col=0; col < cols; col++) {
    			gridCopy.set(row, col, grid.get(indexOf(row, col)));
    		}
		}
        return gridCopy;
    }
    
 }
    

