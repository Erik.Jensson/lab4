package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

	
IGrid currentGeneration;
	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for(int row=0; row<numberOfRows(); row++) {
			for(int col=0; col<numberOfColumns(); col++) {
				nextGeneration.set(row, col, this.getNextCell(row, col));
			}
		}
	}

	@Override
	public CellState getNextCell(int row, int col) {
		if(getCellState(row, col).equals(CellState.ALIVE))
			return CellState.DYING;
		if(getCellState(row, col).equals(CellState.DYING))
			return CellState.DEAD;
		if(countNeighbors(row, col, CellState.ALIVE) == 2 && getCellState(row, col).equals(CellState.DEAD))		
			return CellState.ALIVE;
		else 
			return CellState.DEAD;
		
				
		}	
	
	

	
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		for(int lrow=row-1; lrow<row+2; lrow++) {
			for(int lcol=col-1; lcol<col+2; lcol++) {
				if ((lrow==row && lcol==col) || lrow >= numberOfRows() || lcol >= numberOfColumns() || lrow < 0 || lcol < 0) 
					continue;
				else {
					if(currentGeneration.get(lrow, lcol).equals(state))
						count += 1;
				}
			}
		}
		return count;
	}
	

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}


